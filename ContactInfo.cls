@RestResource(urlMapping='/Contacts/*')
global with sharing class ContactInfo {

@HttpGet
global static Contact getContactsById() {
RestRequest req = RestContext.request;
String ContactId = req.requestURI.substring(
req.requestURI.lastIndexOf('/')+1);
Contact result =
[SELECT FirstName, LastName, 
Department, Email
FROM Contact
WHERE Id = :ContactId];
return result;
}
@HttpPost
global static String createContacts(String fname,
String lname ) {
Contact c = new Contact(
FirstName=fname,
LastName=lname);
insert c;
//return c.Id;
return 'Success';
}
}